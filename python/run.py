#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*--------------------------*-

from level_1 import app
from level_2 import app2

from lib.functions import ask_level

if __name__ == "__main__":
    run_level = ask_level()
    if run_level == 1:
        app.run(debug=False, port=3000)
    else:
        app2.run(debug=False, port=3000)
