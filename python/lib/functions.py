# !/usr/bin/env python
# -*- coding: utf-8 -*-
# -*--------------------------*-

from config import list_json, level_list
import re
import json
from level_2.slow_computation import compute



def parse_to_dict(data):
    """
    search in string the matching data from config list_json. extract them and store them as key / value in returned
    dict
    """
    result = {}
    for item in list_json:
        regex_find = re.compile(r"(" + item + "=.)([^\s]+)")
        match = regex_find.search(data).group()
        result[item] = match.lstrip(item + "= ")
    return result


def to_json_file(data, id_number):
    """
    parse the data and store it in json file named by id_number
    """
    result = parse_to_dict(data)
    with open("./parsed/" + str(id_number) + ".json", "w") as outfile:
        json.dump(result, outfile)
        outfile.close()


def to_redis_list(data, id_number, rs):
    """
    parse the data and enrich it with compute. then store it to a redis list
    """
    result = parse_to_dict(data)
    result = compute(result)
    rs.jsonset(str(id_number), '.', result)


def ask_level():
    """
    choose a level to run between some in a list in config file
    :return: level choose
    """
    while True:
        try:
            run_level = input("What http level do you want to run beetween " + ', '.join(level_list) + " ?\n")
        except ValueError:
            print("Sorry, I didn't understand that.")
            continue
        else:
            if run_level in level_list:
                break
            else:
                print("ERROR : Please select a number between those " + ', '.join(level_list) + " ?")
    return int(run_level)

