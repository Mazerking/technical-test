#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*--------------------------*-

from flask import Flask, render_template, request
import uuid
from lib import log_generator
from lib import functions

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        id_number = uuid.uuid4()
        data = log_generator.sample(id_number)
        functions.to_json_file(data, id_number)
    return render_template('index.html')
