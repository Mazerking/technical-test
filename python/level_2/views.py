#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*--------------------------*-

from flask import Flask, render_template, request
import uuid
import rejson
from celery import Celery


from lib import log_generator
from lib import functions


app2 = Flask(__name__)

# Redis broker URL
app2.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app2.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

# Initialize Celery
celery = Celery(app2.name, broker=app2.config['CELERY_BROKER_URL'])
celery.conf.update(app2.config)
celery.config_from_object(__name__)


@celery.task
def push_to_redis():
    id_number = uuid.uuid4()
    rs = rejson.Client()
    data = log_generator.sample(id_number)
    functions.to_redis_list(data, id_number, rs)


@app2.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        # uncomment and comment here to try with celery solution. see indication.md
        # push_to_redis.delay()
        rs = rejson.Client()
        id_number = uuid.uuid4()
        data = log_generator.sample(id_number)
        functions.to_redis_list(data, id_number, rs)
    return render_template('index.html')
