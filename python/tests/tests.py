#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*--------------------------*-

import unittest

from parameterized import parameterized
from python.lib.functions import parse_to_dict
import rejson


# not working because of weird imports due to run.py. Can be change manually if needed
class functionstests(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(functionstests, self).__init__(*args, **kwargs)

    def setUp(self):
        pass

    def tearDown(self):
        pass


    @parameterized.expand([

        ["id=5096eeb0-9555-4770-9ca7-85fb1f490e6a service_name=web process=web.946 sample#load_avg_1m=0.345 "
         "sample#load_avg_5m=0.251 sample#load_avg_15m=0.775",
         {'id': '5096eeb0-9555-4770-9ca7-85fb1f490e6a', 'service_name': 'web', 'process': 'web.946', 'load_avg_1m':
             '0.345', 'load_avg_5m': '0.251', 'load_avg_15m': '0.775'}],
        ["id= 5096eeb0-9555-4770-9ca7-85fb1f490e6a service_name=web process=web.946 sample#load_avg_1m=0.345 "
         "sample#load_avg_5m=0.251 sample#load_avg_15m=0.775",
         {'id': '5096eeb0-9555-4770-9ca7-85fb1f490e6a', 'service_name': 'web', 'process': 'web.946', 'load_avg_1m':
             '0.345', 'load_avg_5m': '0.251', 'load_avg_15m': '0.775'}],
        ["id= 5096eeb0-9555-4770-9ca7-85fb1f490e6a service_name= web process= web.946 sample#load_avg_1m=0.345 "
         "sample#load_avg_5m=0.251 sample#load_avg_15m= 0.775",
         {'id': '5096eeb0-9555-4770-9ca7-85fb1f490e6a', 'service_name': 'web', 'process': 'web.946', 'load_avg_1m':
             '0.345', 'load_avg_5m': '0.251', 'load_avg_15m': '0.775'}],

    ])
    def test_parse_to_dict(self, data, expected_result):
        result = parse_to_dict(data)
        self.assertEqual(result, expected_result)

    # def test_redis_connection(self):
    #     rs = rejson.Client(host='localhost', port=6379)
    #     self.assertTrue(rs)


if __name__ == '__main__':
    unittest.main()
