# Documentation / indication

## requierements

1 / Python 3.7

2 / A redis server with json module installed 

i personally used it from a docker container see https://hub.docker.com/r/redislabs/rejson/

```docker pull redislabs/rejson```

```docker run -p 6379:6379 --name redis-redisjson redislabs/rejson:latest```

## Configuration file 

list_json = the list of data you are looking for in your logs

level_list = the number of http server level available to launch with run.py

## Setup

`pip install -r requirements.txt`

## Guideline

In a first prompt launch run.py and select witch http server level you want to run.

then you can execute level_http.py in another prompt while the http server is still running

once you've tested level 1 you can start again this process and choose level 2 by running run.py

## Possible Improvement

- Add some var in conf file to make the project more extensible

- example : select the port and the db connections

- import are looking weird in py files because the project is meant to be launch from run.py.

- Create a setup file that display a menu where you can choose to:

    - 1 install dependencies
    - 2 launch level 1 -> start http server + launch levels_http
    - 3 launch level 2 -> start http server + launch worker + launch levels_http

to realise the setup, this one should be able to launche different program 
and kill them when unneed anymore (killing the PID for example)

## Explication

### Level 1

Utilisation de Regex + dico clé valeur.

### Level 2

Utilisation des meme parser que le level 1 + ajout slow compute + push en base redis

Pour atteindre l'objectif du timeout < 0.1s il fallait que je mette en place 
un systéme de queuing. 

Je me suis tourné vers des solutions comme Celery ou RQ.

Hors j'ai réalisé ce projet sur windows. et les worker des différentes 
solutions ne fonctionnaient pas avec cette environement

j'ai tenté de faire tourner des worker sur des machines sous docker, mais
ceux ci ne traitaient pas les tâches provenant de ma machine.

Sans le time.sleep(3) ma solution passe les tests. Je pense que ce qui me manque pour
valider ce niveau est un systéme de queue éfficace.

j'ai laissé le code pour faire théoriquement fonctionner le celery il suffira de le décomenter,
commenter le code non nescessaire. 
Lancer le woker celeri `$ celery -A tasks worker --loglevel=INFO` et enfin lancer le 
serveur 2 et les tests http.

ce soucis m'a fait perdre la majeure partie de mon temps.

### Bonus

https://medium.com/@huseinzolkepli/how-to-ci-cd-flask-app-using-gitlab-ci-4297017acda1

j'aurais suivis ce tuto sur gitlab afin de mettre en place des jobs s'assurant que
mes test sont validés.

Finalement je créerai un dockerfile afin de lancer facilement le projet sous une machine
linux. je setuperai le projet puis lancerai le fichier setup.py 
(voir amélioration / improvement) si celui-ci existait via docker compose
